# Changelog

## 1.0.0

- Initialize React project with Javascript template.
- This project will use the following libraries/frameworks:
  - [`axios`](https://www.npmjs.com/package/axios) in order to make http requests to the backend.
  - [`socket.io-client`](https://www.npmjs.com/package/socket.io-client) in order to set a connection by sockets.
  - [`@material-ui/core`](https://www.npmjs.com/package/@material-ui/core) in order to create the stylized components.
  - [`@material-ui/icons`](https://www.npmjs.com/package/@material-ui/icons) in order to use Material icons.
  - [`redux`](https://www.npmjs.com/package/redux) in order to manage the state of the application.
  - [`react-redux`](https://www.npmjs.com/package/react-redux) in order to integrate redux with react.
  - [`redux-saga`](https://www.npmjs.com/package/redux-saga) in order to make asynchronous things like data fetching, etc.
  - [`react-router-dom`](https://www.npmjs.com/package/react-router-dom) in order to manage the routes and history.
  - [`react-beautiful-dnd`](https://www.npmjs.com/package/react-beautiful-dnd) in order to create draggable components.
  - [`react-chartjs-2`](https://www.npmjs.com/package/react-chartjs-2) in order to create charts for the data.
  - [`validate.js`](https://www.npmjs.com/package/validate.js) in order to validate form fields.
  - [`firebase`](https://www.npmjs.com/package/firebase) in order to use [`Firebase`](https://firebase.google.com/?hl=es).
  - [`react-redux-firebase`](https://www.npmjs.com/package/react-redux-firebase) in order to integrate firebase with react and redux.
