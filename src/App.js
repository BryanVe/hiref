import React from 'react';
import { useSelector } from 'react-redux';
import { isLoaded } from 'react-redux-firebase';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Loading } from 'components';
import routes from 'routes';

const AuthIsLoaded = ({ children }) => {
  const auth = useSelector((state) => state.firebase.auth);
  if (!isLoaded(auth)) return <Loading />;
  return children;
};

const ProfileIsLoaded = ({ children }) => {
  const profile = useSelector((state) => state.firebase.profile);
  if (!isLoaded(profile)) return <Loading />;
  return children;
};

const App = () => {
  return (
    <AuthIsLoaded>
      <ProfileIsLoaded>
        <Switch>
          <Route
            exact
            path="/"
            component={() => <Redirect to="/auth/signin" />}
          />

          {routes.map(({ layout: Layout, views }) =>
            views.map(({ path, component: Component }) => (
              <Route
                key={path}
                exact
                path={path}
                render={() => (
                  <Layout>
                    <Component />
                  </Layout>
                )}
              />
            ))
          )}
          <Route component={() => <Redirect to="/error/404" />} />
        </Switch>
      </ProfileIsLoaded>
    </AuthIsLoaded>
  );
};

export default App;
