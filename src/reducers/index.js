import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';
import { firestoreReducer } from 'redux-firestore';

import jobOffersReducer from './jobOffers';
import projectsReducer from './projects';

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer,
  jobOffers: jobOffersReducer,
  projects: projectsReducer,
});

export default rootReducer;
