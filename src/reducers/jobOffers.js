import {
  GET_JOB_OFFERS_BY_OCCUPATIONS,
  GET_ALL_JOB_OFFERS,
  GET_JOB_OFFERS_BY_PROJECT,
  CREATE_JOB_OFFER_BY_CLIENT,
} from 'constants/index';

const initialState = {
  loading: false,
  error: '',
  data: [],
};

const jobOffersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_JOB_OFFERS_BY_OCCUPATIONS.REQUESTED:
    case GET_ALL_JOB_OFFERS.REQUESTED:
    case GET_JOB_OFFERS_BY_PROJECT.REQUESTED:
    case CREATE_JOB_OFFER_BY_CLIENT.REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case GET_JOB_OFFERS_BY_OCCUPATIONS.SUCCESS:
    case GET_ALL_JOB_OFFERS.SUCCESS:
    case GET_JOB_OFFERS_BY_PROJECT.SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.jobOffers,
      };

    case CREATE_JOB_OFFER_BY_CLIENT.SUCCESS:
      return {
        ...state,
        loading: false,
        data: [...state.data, action.payload.jobOffer],
      };
    case GET_JOB_OFFERS_BY_OCCUPATIONS.ERROR:
    case GET_ALL_JOB_OFFERS.ERROR:
    case GET_JOB_OFFERS_BY_PROJECT.ERROR:
    case CREATE_JOB_OFFER_BY_CLIENT.ERROR:
      return {
        ...state,
        loading: false,
        data: action.payload.message,
      };
    default:
      return state;
  }
};

export default jobOffersReducer;
