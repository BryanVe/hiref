import {
  GET_PROJECTS_BY_CLIENT,
  CREATE_PROJECT_BY_CLIENT,
  GET_PROJECT_BY_ID,
} from 'constants/index';

const initialState = {
  loading: false,
  error: '',
  currentProject: {},
  data: [],
};

const projectsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROJECTS_BY_CLIENT.REQUESTED:
    case CREATE_PROJECT_BY_CLIENT.REQUESTED:
    case GET_PROJECT_BY_ID.REQUESTED:
      return {
        ...state,
        loading: true,
      };

    case GET_PROJECTS_BY_CLIENT.SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.projects,
      };

    case GET_PROJECT_BY_ID.SUCCESS:
      return {
        ...state,
        loading: false,
        currentProject: action.payload.currentProject,
      };
    case CREATE_PROJECT_BY_CLIENT.SUCCESS:
      return {
        ...state,
        loading: false,
        data: [...state.data, action.payload.newProject],
      };
    case GET_PROJECTS_BY_CLIENT.ERROR:
    case CREATE_PROJECT_BY_CLIENT.ERROR:
    case GET_PROJECT_BY_ID.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      };

    default:
      return state;
  }
};

export default projectsReducer;
