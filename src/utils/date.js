export const getDateFromUTC = (utc) => new Date(utc).toDateString();
