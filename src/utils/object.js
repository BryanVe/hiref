export const isEmptyObject = (object) => {
  return Object.values(object).length === 0;
};

export const getUserDataFromObject = (object) =>
  Object.keys(object).map((key) => ({
    uid: key,
    ...object[key],
  }));
