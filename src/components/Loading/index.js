import React from 'react';
import { CircularProgress } from '@material-ui/core';

const Loading = () => {
  return (
    <div
      style={{
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <CircularProgress style={{ margin: '0 auto' }} color="inherit" />
    </div>
  );
};

export default Loading;
