import { custom } from '../palette';

export default {
  paper: {
    backgroundColor: custom.gray.main,
  },
};
