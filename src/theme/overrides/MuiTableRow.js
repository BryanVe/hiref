import palette from '../palette';

export default {
  root: {
    fontFamily: 'Quicksand',
    '&$selected': {
      backgroundColor: palette.background.default,
    },
    '&$hover': {
      '&:hover': {
        backgroundColor: palette.background.default,
      },
    },
  },
};
