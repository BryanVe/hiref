import {
  GET_JOB_OFFERS_BY_OCCUPATIONS,
  GET_ALL_JOB_OFFERS,
  GET_JOB_OFFERS_BY_PROJECT,
  CREATE_JOB_OFFER_BY_CLIENT,
} from 'constants/index';

export const getJobOffersByOccupationsRequested = (occupations) => ({
  type: GET_JOB_OFFERS_BY_OCCUPATIONS.REQUESTED,
  payload: { occupations },
});

export const getJobOffersByOccupationsSuccess = (jobOffers) => ({
  type: GET_JOB_OFFERS_BY_OCCUPATIONS.SUCCESS,
  payload: { jobOffers },
});

export const getJobOffersByOccupationsError = (message) => ({
  type: GET_JOB_OFFERS_BY_OCCUPATIONS.ERROR,
  payload: { message },
});

export const getJobOffersByProjectRequested = (projectId) => ({
  type: GET_JOB_OFFERS_BY_PROJECT.REQUESTED,
  payload: { projectId },
});

export const getJobOffersByProjectSuccess = (jobOffers) => ({
  type: GET_JOB_OFFERS_BY_PROJECT.SUCCESS,
  payload: { jobOffers },
});

export const getJobOffersByProjectError = (message) => ({
  type: GET_JOB_OFFERS_BY_PROJECT.ERROR,
  payload: { message },
});

export const getAllJobOffersRequested = () => ({
  type: GET_ALL_JOB_OFFERS.REQUESTED,
});

export const getAllJobOffersSuccess = (jobOffers) => ({
  type: GET_ALL_JOB_OFFERS.SUCCESS,
  payload: { jobOffers },
});

export const getAllJobOffersError = (message) => ({
  type: GET_ALL_JOB_OFFERS.ERROR,
  payload: { message },
});

export const createJobOfferByClientRequested = (projectId, newJobOffer) => ({
  type: CREATE_JOB_OFFER_BY_CLIENT.REQUESTED,
  payload: { projectId, newJobOffer },
});

export const createJobOfferByClientSuccess = (jobOffer) => ({
  type: CREATE_JOB_OFFER_BY_CLIENT.SUCCESS,
  payload: { jobOffer },
});

export const createJobOfferByClientError = (message) => ({
  type: CREATE_JOB_OFFER_BY_CLIENT.ERROR,
  payload: { message },
});
