import {
  GET_PROJECTS_BY_CLIENT,
  CREATE_PROJECT_BY_CLIENT,
  GET_PROJECT_BY_ID,
} from 'constants/index';

export const getProjectsByClientRequested = (idClient) => ({
  type: GET_PROJECTS_BY_CLIENT.REQUESTED,
  payload: { idClient },
});

export const getProjectsByClientSuccess = (projects) => ({
  type: GET_PROJECTS_BY_CLIENT.SUCCESS,
  payload: { projects },
});

export const getProjectsByClientError = (message) => ({
  type: GET_PROJECTS_BY_CLIENT.ERROR,
  payload: { message },
});

export const getProjectByIdRequested = (projectId) => ({
  type: GET_PROJECT_BY_ID.REQUESTED,
  payload: { projectId },
});

export const getProjectByIdSuccess = (currentProject) => ({
  type: GET_PROJECT_BY_ID.SUCCESS,
  payload: { currentProject },
});

export const getProjectByIdError = (message) => ({
  type: GET_PROJECT_BY_ID.ERROR,
  payload: { message },
});

export const createProjectByClientRequested = (idClient, newProject) => ({
  type: CREATE_PROJECT_BY_CLIENT.REQUESTED,
  payload: { idClient, newProject },
});

export const createProjectByClientSuccess = (newProject) => ({
  type: CREATE_PROJECT_BY_CLIENT.SUCCESS,
  payload: { newProject },
});

export const createProjectByClientError = (message) => ({
  type: CREATE_PROJECT_BY_CLIENT.ERROR,
  payload: { message },
});
