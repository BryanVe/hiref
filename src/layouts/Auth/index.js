import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  content: {
    backgroundColor: '#F2F2F2',
  },
}));

const Auth = ({ children }) => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <main className={classes.content}>{children}</main>
    </React.Fragment>
  );
};

export default Auth;
