import React from 'react';

import GeneralLayout from 'layouts/General';
import navigationConfig from './components/NavBar/navigationConfig';

const ClientLayout = ({ children }) => (
  <GeneralLayout roleLabel="Client" navigationConfig={navigationConfig}>
    {children}
  </GeneralLayout>
);

export default ClientLayout;
