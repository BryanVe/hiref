import PostAddRoundedIcon from '@material-ui/icons/PostAddRounded';
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import CheckRoundedIcon from '@material-ui/icons/CheckRounded';
import DoneAllRoundedIcon from '@material-ui/icons/DoneAllRounded';
import SupervisorAccountRoundedIcon from '@material-ui/icons/SupervisorAccountRounded';
import PaymentRoundedIcon from '@material-ui/icons/PaymentRounded';

export default [
  {
    title: 'Work space',
    pages: [
      {
        title: 'Projects',
        href: '/client/projects',
        icon: PostAddRoundedIcon,
        children: [
          {
            title: 'Active',
            href: '/client/projects/active',
            icon: CheckRoundedIcon,
          },
          {
            title: 'Completed',
            href: '/client/projects/completed',
            icon: DoneAllRoundedIcon,
          },
          {
            title: 'Canceled',
            href: '/client/projects/canceled',
            icon: ClearRoundedIcon,
          },
        ],
      },
      {
        title: 'Manage supervisors',
        href: '/client/manage-supervisors',
        icon: SupervisorAccountRoundedIcon,
      },
      {
        title: 'Pay membership ',
        href: '/client/pay-membership',
        icon: PaymentRoundedIcon,
      },
    ],
  },
];
