export { default as AdminLayout } from './Admin';
export { default as AspirantLayout } from './Aspirant';
export { default as AuthLayout } from './Auth';
export { default as ErrorLayout } from './Error';
export { default as CommercialAreaLayout } from './CommercialArea';
export { default as ClientLayout } from './Client';
export { default as EvaluatorLayout } from './Evaluator';
