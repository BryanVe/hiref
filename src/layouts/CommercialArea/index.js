import React from 'react';

import GeneralLayout from 'layouts/General';
import navigationConfig from './components/NavBar/navigationConfig';

const CommercialAreaLayout = ({ children }) => (
  <GeneralLayout
    roleLabel="Commercial Area"
    navigationConfig={navigationConfig}
  >
    {children}
  </GeneralLayout>
);

export default CommercialAreaLayout;
