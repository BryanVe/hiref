import ApartmentRoundedIcon from '@material-ui/icons/ApartmentRounded';

export default [
  {
    title: 'Manage users',
    pages: [
      {
        title: 'Manage clients',
        href: '/commercial-area/manage-clients',
        icon: ApartmentRoundedIcon,
      },
    ],
  },
];
