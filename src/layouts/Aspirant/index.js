import React from 'react';

import GeneralLayout from 'layouts/General';
import navigationConfig from './components/NavBar/navigationConfig';

const AspirantLayout = ({ children }) => (
  <GeneralLayout roleLabel="Aspirant" navigationConfig={navigationConfig}>
    {children}
  </GeneralLayout>
);

export default AspirantLayout;
