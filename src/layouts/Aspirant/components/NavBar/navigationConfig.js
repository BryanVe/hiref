import WorkRoundedIcon from '@material-ui/icons/WorkRounded';
import PageviewRoundedIcon from '@material-ui/icons/PageviewRounded';
import SendRoundedIcon from '@material-ui/icons/SendRounded';
// import ThumbDownRoundedIcon from '@material-ui/icons/ThumbDownRounded';

export default [
  {
    title: 'Work space',
    pages: [
      {
        title: 'Job offers',
        href: '/aspirant/job-offers',
        icon: WorkRoundedIcon,
        children: [
          {
            title: 'Available',
            href: '/aspirant/job-offers/available',
            icon: PageviewRoundedIcon,
          },
          {
            title: 'Applied',
            href: '/aspirant/job-offers/applied',
            icon: SendRoundedIcon,
          },
          // {
          //   title: 'Rejected',
          //   href: '/aspirant/job-offers/rejected',
          //   icon: ThumbDownRoundedIcon,
          // },
        ],
      },
    ],
  },
];
