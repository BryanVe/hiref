import SupervisorAccountRoundedIcon from '@material-ui/icons/SupervisorAccountRounded';

export default [
  {
    title: 'Manage',
    pages: [
      {
        title: 'Manage users',
        href: '/admin/manage-users',
        icon: SupervisorAccountRoundedIcon,
      },
    ],
  },
];
