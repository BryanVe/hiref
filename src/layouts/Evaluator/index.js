import React from 'react';

import GeneralLayout from 'layouts/General';
import navigationConfig from './components/NavBar/navigationConfig';

const EvaluatorLayout = ({ children }) => (
  <GeneralLayout roleLabel="Evaluator" navigationConfig={navigationConfig}>
    {children}
  </GeneralLayout>
);

export default EvaluatorLayout;
