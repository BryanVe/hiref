import WorkRoundedIcon from '@material-ui/icons/WorkRounded';
import PageviewRoundedIcon from '@material-ui/icons/PageviewRounded';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import DoneAllRoundedIcon from '@material-ui/icons/DoneAllRounded';

export default [
  {
    title: 'Work space',
    pages: [
      {
        title: 'Job offers',
        href: '/evaluator/job-offers',
        icon: WorkRoundedIcon,
        children: [
          {
            title: 'Available',
            href: '/evaluator/job-offers/available',
            icon: PageviewRoundedIcon,
          },
          {
            title: 'In evaluation',
            href: '/evaluator/job-offers/in-evaluation',
            icon: BorderColorRoundedIcon,
          },
          {
            title: 'Completed',
            href: '/evaluator/job-offers/completed',
            icon: DoneAllRoundedIcon,
          },
        ],
      },
    ],
  },
];
