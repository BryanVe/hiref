import React, { useState } from 'react';
import { TableWithIcons, Loading } from 'components';
import { useFirestoreConnect } from 'react-redux-firebase';
import { useSelector } from 'react-redux';

import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import DeleteOutlineRoundedIcon from '@material-ui/icons/DeleteOutlineRounded';
import { isEmptyObject, getUserDataFromObject } from 'utils';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Button,
  DialogContentText,
  DialogActions,
  makeStyles,
  TextField,
  Grid,
  useTheme,
  useMediaQuery,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  deleteButton: {
    backgroundColor: theme.palette.error.main,
    color: theme.palette.custom.white,
    borderColor: theme.palette.error.dark,
    '&:hover': {
      backgroundColor: theme.palette.error.light,
    },
  },
  saveButton: {
    padding: theme.spacing(0.5, 4),
  },
  withoutOverflow: {
    overflowY: 'initial',
  },
}));

const ClientsTable = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const classes = useStyles();
  useFirestoreConnect([
    {
      collection: 'users',
      where: ['role', '==', 'client'],
    },
  ]);
  const [
    openDeleteConfirmationDialog,
    setOpenDeleteConfirmationDialog,
  ] = useState(false);
  const [openEditCredentialsDialog, setOpenEditCredentialsDialog] = useState(
    false
  );
  const [openCreateClientDialog, setOpenCreateClientDialog] = useState(false);
  const data = useSelector((state) => state.firestore.data);

  const columns = [
    { title: 'Business name', field: 'business_name' },
    { title: 'Commercial name', field: 'commercial_name' },
    { title: 'RUC', field: 'ruc' },
  ];

  const createNewClient = () => {
    // alert('New user created');
    handleOpenCreateClientDialog();
  };

  const editClientSelected = (event, rowData) => {
    // alert('Client selected has been edited');
    handleOpenEditCredentialsDialog();
  };

  const deleteClientSelected = (event, rowData) => {
    // console.log(rowData)
    handleOpenDeleteConfirmationDialog();
  };

  const handleOpenDeleteConfirmationDialog = () =>
    setOpenDeleteConfirmationDialog(true);

  const handleCloseDeleteConfirmationDialog = () =>
    setOpenDeleteConfirmationDialog(false);

  const handleOpenEditCredentialsDialog = () =>
    setOpenEditCredentialsDialog(true);

  const handleCloseEditCredentialsDialog = () =>
    setOpenEditCredentialsDialog(false);

  const handleOpenCreateClientDialog = () => setOpenCreateClientDialog(true);

  const handleCloseCreateClientDialog = () => setOpenCreateClientDialog(false);

  const getDeleteClientDialog = () => (
    <Dialog
      open={openDeleteConfirmationDialog}
      onClose={handleCloseDeleteConfirmationDialog}
      aria-labelledby="delete-confirmation-dialog-title"
      aria-describedby="delete-confirmation-dialog-description"
    >
      <DialogTitle id="delete-confirmation-dialog-title">
        Are you absolutely sure?
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="delete-confirmation-dialog-description">
          This action <b>cannot</b> be undone. This will permanently delete the
          client selected
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleCloseDeleteConfirmationDialog}
          variant="contained"
          autoFocus
          className={classes.deleteButton}
        >
          Delete
        </Button>
        <Button
          onClick={handleCloseDeleteConfirmationDialog}
          variant="outlined"
          color="primary"
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );

  const getEditCredentialsDialog = () => (
    <Dialog
      maxWidth="xs"
      open={openEditCredentialsDialog}
      onClose={handleCloseEditCredentialsDialog}
      aria-labelledby="edit-credentials-dialog-title"
      aria-describedby="edit-credentials-dialog-description"
    >
      <DialogTitle id="edit-credentials-dialog-title">
        Edit access credentials
      </DialogTitle>
      <DialogContent className={classes.withoutOverflow}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              fullWidth
              size="small"
              label="Email"
              variant="outlined"
              type="email"
            />
          </Grid>
          <Grid item container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField
                fullWidth
                size="small"
                label="Password"
                variant="outlined"
                type="password"
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                fullWidth
                size="small"
                label="Confirm password"
                variant="outlined"
                type="password"
              />
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          fullWidth={isMobile}
          className={classes.saveButton}
          onClick={handleCloseEditCredentialsDialog}
          variant="contained"
          color="primary"
          autoFocus
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );

  const getCreateClientDialog = () => (
    <Dialog
      maxWidth="xs"
      open={openCreateClientDialog}
      onClose={handleCloseCreateClientDialog}
      aria-labelledby="create-client-dialog-title"
      aria-describedby="create-client-dialog-description"
    >
      <DialogTitle id="create-client-dialog-title">
        Create new client of <b>hiref</b>
      </DialogTitle>
      <DialogContent className={classes.withoutOverflow}>
        <Grid container spacing={2}>
          <Grid item container spacing={2}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                size="small"
                label="Commercial name"
                variant="outlined"
                type="text"
              />
            </Grid>
            <Grid item container spacing={2}>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="Business name"
                  variant="outlined"
                  type="text"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="RUC"
                  variant="outlined"
                  type="text"
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                size="small"
                label="Email"
                variant="outlined"
                type="email"
              />
            </Grid>
            <Grid item container spacing={2}>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="Password"
                  variant="outlined"
                  type="password"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="Confirm password"
                  variant="outlined"
                  type="password"
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          fullWidth={isMobile}
          className={classes.saveButton}
          onClick={handleCloseCreateClientDialog}
          variant="contained"
          color="primary"
          autoFocus
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );

  return isEmptyObject(data) ? (
    <Loading />
  ) : (
    <React.Fragment>
      <TableWithIcons
        title=""
        columns={columns}
        data={getUserDataFromObject(data.users)}
        actions={[
          {
            icon: () => <AddCircleOutlineRoundedIcon />,
            tooltip: 'Add new client',
            isFreeAction: true,
            onClick: createNewClient,
          },
          {
            icon: () => <EditRoundedIcon />,
            tooltip: 'Edit access credentials',
            onClick: editClientSelected,
          },
          {
            icon: () => <DeleteOutlineRoundedIcon />,
            tooltip: 'Delete client',
            onClick: deleteClientSelected,
          },
        ]}
      />
      {getDeleteClientDialog()}
      {getEditCredentialsDialog()}
      {getCreateClientDialog()}
    </React.Fragment>
  );
};

export default ClientsTable;
