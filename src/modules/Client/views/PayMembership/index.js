import React, { useState } from 'react';
import withUserAsRole from 'hocs/withUserAsRole';
import {
  Grid,
  Typography,
  TextField,
  CardContent,
  Card,
  makeStyles,
} from '@material-ui/core';
import { ToggleButtonGroup, ToggleButton } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
  headerTitle: {
    marginBottom: 20,
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
  },
}));

const PayMembership = () => {
  const classes = useStyles();
  const [cardOption, setCardOption] = useState('visa');

  const handleCardOption = (event, cardOptionSelected) => {
    console.log(event);
    console.log(cardOptionSelected);
    setCardOption(cardOptionSelected);
  };

  return (
    <React.Fragment>
      <Typography variant="h1" className={classes.headerTitle}>
        Pay membership: 15 days remaining
      </Typography>
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md={2}>
              <ToggleButtonGroup
                size="large"
                value={cardOption}
                exclusive
                onChange={handleCardOption}
              >
                <ToggleButton value="visa">Visa</ToggleButton>
                <ToggleButton value="mastercard">Mastercard</ToggleButton>
              </ToggleButtonGroup>
            </Grid>
            <Grid item xs={12} md={5}>
              <TextField
                fullWidth
                label="Card number"
                variant="outlined"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={3}>
              <TextField
                fullWidth
                label="Due date"
                variant="outlined"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={2}>
              <TextField fullWidth label="CVV" variant="outlined" type="text" />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </React.Fragment>
  );
};

export default withUserAsRole('client', PayMembership);
