import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import withUserAsRole from 'hocs/withUserAsRole';

import { useSelector, useDispatch } from 'react-redux';
import { Loading } from 'components';
import {
  getProjectByIdRequested,
  getJobOffersByProjectRequested,
} from 'actions';
import { useFirestoreConnect } from 'react-redux-firebase';
import { isEmptyObject } from 'utils';
import ProjectDetails from './ProjectDetails';

const ProjectActiveDetails = () => {
  useFirestoreConnect([
    {
      collection: 'users',
      where: ['role', '==', 'supervisor'],
    },
  ]);
  const { projectId } = useParams();
  const dispatch = useDispatch();
  const { loading: loadingProjects } = useSelector((state) => state.projects);
  const { loading: loadingJobOffers } = useSelector((state) => state.jobOffers);
  const data = useSelector((state) => state.firestore.data);

  useEffect(() => {
    dispatch(getProjectByIdRequested(projectId));
    dispatch(getJobOffersByProjectRequested(projectId));
  }, [dispatch, projectId]);

  return loadingProjects || loadingJobOffers || isEmptyObject(data) ? (
    <Loading />
  ) : (
    <ProjectDetails projectId={projectId} />
  );
};

export default withUserAsRole('client', ProjectActiveDetails);
