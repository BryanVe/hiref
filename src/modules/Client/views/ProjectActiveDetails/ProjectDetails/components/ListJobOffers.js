import React from 'react';
import {
  Typography,
  CardContent,
  Card,
  Chip,
  makeStyles,
  useTheme,
  useMediaQuery,
} from '@material-ui/core';

import { getDateFromUTC } from 'utils';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100%',
  },
  cardHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  cardDescription: {
    margin: theme.spacing(1.5, 0),
  },
  content: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  clientLogo: {
    minWidth: 160,
    maxWidth: 160,
  },

  deadlineChipWrapper: {
    textAlign: 'center',
  },
  dateChip: {
    color: theme.palette.custom.white,
    fontWeight: 'bold',
    backgroundColor: theme.palette.error.light,
  },
  detailsButton: {
    marginLeft: 'auto',
  },
  detailsDrawerContent: {
    backgroundColor: theme.palette.custom.white,
    width: '70vw',
    [theme.breakpoints.down('sm')]: {
      width: '90vw',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100vw',
    },
  },
  clientLogoDetails: {
    width: 150,
    height: 150,
    [theme.breakpoints.down('sm')]: {
      width: 120,
      height: 120,
    },
  },
  applicantsVsTotalChip: {
    backgroundColor: theme.palette.success.light,
    color: theme.palette.custom.white,
    fontWeight: 'bold',
    marginTop: theme.spacing(1),
  },
  contentTab: {
    backgroundColor: theme.palette.custom.white,
  },
}));

const getNumberOfAppliesVsTotal = (roles) => {
  const versus = {
    applicants: 0,
    total: 0,
  };
  for (let role of roles) {
    versus.applicants += role.applicants.length;
    versus.total += role.quantity;
  }
  return `${versus.applicants} / ${versus.total}`;
};

const ListJobOffers = ({
  jobOffer: { description, deadline, occupations, roles, status },
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('md'));

  const getApplicantsVsTotalChip = () => (
    <Chip
      size={isMobile ? 'small' : 'medium'}
      label={getNumberOfAppliesVsTotal(roles)}
      className={classes.applicantsVsTotalChip}
    />
  );

  return (
    <React.Fragment>
      <Card className={classes.root}>
        <CardContent className={classes.content}>
          <div className={classes.cardHeader}>
            <div>
              <Typography variant="h3">{description.title}</Typography>
              {getApplicantsVsTotalChip()}
            </div>
            <div className={classes.deadlineChipWrapper}>
              <Typography variant="subtitle2">Expires</Typography>
              <Chip
                label={getDateFromUTC(deadline)}
                size={isMobile ? 'small' : 'medium'}
                className={classes.dateChip}
              />
            </div>
          </div>
          <Typography variant="subtitle2" className={classes.cardDescription}>
            {description.content}
          </Typography>
          {/* <Button
            variant="outlined"
            color="primary"
            className={classes.detailsButton}
            onClick={handleOpenDetailsDrawer}
          >
            Details
          </Button> */}
        </CardContent>
      </Card>
    </React.Fragment>
  );
};

export default ListJobOffers;
