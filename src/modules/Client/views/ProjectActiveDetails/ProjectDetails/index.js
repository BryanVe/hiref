import React, { useState } from 'react';
import {
  IconButton,
  Typography,
  makeStyles,
  Chip,
  Card,
  CardContent,
  useMediaQuery,
  useTheme,
  Grid,
  Paper,
  Divider,
  Tooltip,
  InputBase,
} from '@material-ui/core';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import SearchIcon from '@material-ui/icons/Search';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';

import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getDateFromUTC, isEmptyObject } from 'utils';
import { Loading } from 'components';
import ListJobOFfers from './components/ListJobOffers';

const useStyles = makeStyles((theme) => ({
  goBack: {
    display: 'flex',
    alignItems: 'center',
  },
  header: {
    margin: '20px 0',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  searchInput: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  headerTitle: {
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
      marginBottom: 10,
    },
  },
}));

const ProjectDetails = ({ projectId }) => {
  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const { currentProject } = useSelector((state) => state.projects);
  const { data: jobOffers } = useSelector((state) => state.jobOffers);
  const data = useSelector((state) => state.firestore.data);
  const [projectSearched, setProjectSearched] = useState('');

  const filteredJobOffers = () =>
    jobOffers.filter(
      (jobOffer) =>
        jobOffer.description.title
          .toLowerCase()
          .indexOf(projectSearched.toLowerCase()) > -1
    );

  const handleOpenCreateNewJobOffer = () =>
    history.push(`/client/projects/active/${projectId}/create-joboffer`);

  return isEmptyObject(currentProject) ? (
    <Loading />
  ) : (
    <React.Fragment>
      <div className={classes.goBack}>
        <IconButton onClick={() => history.push('/client/projects/active')}>
          <ArrowBackRoundedIcon />
        </IconButton>
        <Typography variant="h4" style={{ marginLeft: 5 }}>
          Back
        </Typography>
      </div>
      <Card className={classes.root}>
        <CardContent className={classes.content}>
          <div className={classes.cardHeader}>
            <Typography variant="h3">
              {currentProject.description.title}
            </Typography>
            <div className={classes.chipWrapper}>
              <Typography variant="subtitle2">Expires</Typography>
              <Chip
                label={getDateFromUTC(currentProject.deadline)}
                size={isMobile ? 'small' : 'medium'}
                className={classes.dateChip}
              />
            </div>
          </div>
          <div>
            <Typography variant="subtitle2" className={classes.cardDescription}>
              {currentProject.description.content}
            </Typography>
          </div>
          <div className={classes.supervisors}>
            {currentProject.supervisors.map(
              (uid) =>
                !!data.users[uid] && (
                  <Chip
                    key={uid}
                    size={isMobile ? 'small' : 'medium'}
                    label={`${data.users[uid].names} ${data.users[uid].lastNames}`}
                    className={classes.supervisorChip}
                  />
                )
            )}
          </div>
        </CardContent>
      </Card>
      <div className={classes.header}>
        <Typography variant="h3" className={classes.headerTitle}>
          Job offers from this project
        </Typography>
        <Paper component="form" className={classes.searchInput}>
          <InputBase
            className={classes.input}
            placeholder="Search job offer"
            inputProps={{ 'aria-label': 'search project' }}
            onChange={(e) => setProjectSearched(e.target.value)}
          />
          <IconButton
            disabled
            className={classes.iconButton}
            aria-label="search"
          >
            <SearchIcon />
          </IconButton>
          <Divider className={classes.divider} />
          <Tooltip title="Create new project">
            <IconButton
              aria-label="create"
              onClick={handleOpenCreateNewJobOffer}
            >
              <AddCircleOutlineRoundedIcon />
            </IconButton>
          </Tooltip>
        </Paper>
      </div>
      {jobOffers.length > 0 ? (
        <React.Fragment>
          {filteredJobOffers().length > 0 ? (
            filteredJobOffers().map((jobOffer) => (
              <Grid key={jobOffer._id} container item xs={12}>
                <ListJobOFfers jobOffer={jobOffer} />
              </Grid>
            ))
          ) : (
            <Typography
              className={classes.header}
              variant={isMobile ? 'h3' : 'h2'}
            >
              No matches
            </Typography>
          )}
        </React.Fragment>
      ) : (
        <Typography className={classes.header} variant={isMobile ? 'h3' : 'h2'}>
          There are no job offers in this project
        </Typography>
      )}
    </React.Fragment>
  );
};

export default ProjectDetails;
