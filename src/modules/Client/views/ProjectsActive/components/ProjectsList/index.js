import React, { useState } from 'react';
import {
  makeStyles,
  Grid,
  Typography,
  Divider,
  InputBase,
  Paper,
  IconButton,
  Tooltip,
  useTheme,
  useMediaQuery,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Button,
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { Autocomplete } from '@material-ui/lab';

import { useSelector, useDispatch } from 'react-redux';
import Loading from 'components/Loading';
import ListElement from './components/ListElement';
import SearchIcon from '@material-ui/icons/Search';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
import { isEmptyObject, getUserDataFromObject } from 'utils';
import { useFirestoreConnect } from 'react-redux-firebase';
import { createProjectByClientRequested } from 'actions';

const initialStateProject = {
  code: '',
  deadline: null,
  description: {
    title: '',
    content: '',
  },
  supervisors: [],
};

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  headerTitle: {
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
      marginBottom: 10,
    },
  },
  searchInput: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  withoutOverflow: {
    overflowY: 'initial',
  },
  saveButton: {
    padding: theme.spacing(0.5, 4),
  },
  deadlinePickerContent: {
    color: theme.palette.custom.white,
  },
}));

const List = () => {
  useFirestoreConnect([
    {
      collection: 'users',
      where: ['role', '==', 'supervisor'],
    },
  ]);
  const dispatch = useDispatch();
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const { loading, data: projects } = useSelector((state) => state.projects);
  const idClient = useSelector((state) => state.firebase.auth.uid);
  const [projectSearched, setProjectSearched] = useState('');
  const [openCreateProjectDialog, setOpenCreateProjectDialog] = useState(false);
  const data = useSelector((state) => state.firestore.data);
  const [newProjectData, setNewProjectData] = useState(initialStateProject);

  const handleOpenCreateProjectDialog = () => setOpenCreateProjectDialog(true);

  const handleCloseCreateProjectDialog = () =>
    setOpenCreateProjectDialog(false);

  const createProject = () => {
    dispatch(createProjectByClientRequested(idClient, newProjectData));
    setNewProjectData(initialStateProject);
    handleCloseCreateProjectDialog();
  };

  const getCreateProjectDialog = () => (
    <Dialog
      maxWidth="sm"
      open={openCreateProjectDialog}
      onClose={handleCloseCreateProjectDialog}
      aria-labelledby="create-project-dialog-title"
      aria-describedby="create-project-dialog-description"
    >
      <DialogTitle id="create-project-dialog-title">
        Create a <b>new project</b>
      </DialogTitle>
      <DialogContent className={classes.withoutOverflow}>
        <Grid container spacing={2}>
          <Grid item container spacing={2}>
            <Grid item xs={12} md={8}>
              <TextField
                fullWidth
                onChange={(event) =>
                  setNewProjectData({
                    ...newProjectData,
                    description: {
                      ...newProjectData.description,
                      title: event.target.value,
                    },
                  })
                }
                size="small"
                label="Title"
                value={newProjectData.description.title}
                variant="outlined"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={4}>
              <TextField
                fullWidth
                size="small"
                label="Code"
                onChange={(event) =>
                  setNewProjectData({
                    ...newProjectData,
                    code: event.target.value,
                  })
                }
                variant="outlined"
                value={newProjectData.code}
                type="text"
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              onChange={(event) =>
                setNewProjectData({
                  ...newProjectData,
                  description: {
                    ...newProjectData.description,
                    content: event.target.value,
                  },
                })
              }
              size="small"
              label="Description"
              multiline
              rows={5}
              value={newProjectData.description.content}
              variant="outlined"
              type="text"
            />
          </Grid>
          <Grid item container spacing={2}>
            <Grid item xs={12} md={6}>
              <Autocomplete
                multiple
                limitTags={2}
                size="small"
                options={getUserDataFromObject(data.users).map(
                  (user) => user.uid
                )}
                getOptionLabel={(uid) =>
                  `${data.users[uid].names} ${data.users[uid].lastNames}`
                }
                onChange={(event, supervisors) => {
                  setNewProjectData({
                    ...newProjectData,
                    supervisors,
                  });
                }}
                // defaultValue={[top100Films[13], top100Films[12], top100Films[11]]}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder="Type a supervisor name"
                    fullWidth
                    size="small"
                    variant="outlined"
                    type="text"
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <KeyboardDatePicker
                fullWidth
                size="small"
                inputVariant="outlined"
                clearable
                value={newProjectData.deadline}
                placeholder="Deadline"
                onChange={(date) =>
                  setNewProjectData({
                    ...newProjectData,
                    deadline: date.format('YYYY-MM-DD'),
                  })
                }
                minDate={new Date()}
                format="DD/MM/YYYY"
              />
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          fullWidth={isMobile}
          className={classes.saveButton}
          onClick={createProject}
          variant="contained"
          color="primary"
          autoFocus
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );

  const filteredProjects = () =>
    projects.filter(
      (project) =>
        project.description.title
          .toLowerCase()
          .indexOf(projectSearched.toLowerCase()) > -1
    );

  return loading || isEmptyObject(data) ? (
    <Loading />
  ) : (
    <React.Fragment>
      <div className={classes.header}>
        <Typography variant="h1" className={classes.headerTitle}>
          Active projects
        </Typography>
        <Paper component="form" className={classes.searchInput}>
          <InputBase
            className={classes.input}
            placeholder="Search project"
            inputProps={{ 'aria-label': 'search project' }}
            onChange={(e) => setProjectSearched(e.target.value)}
          />
          <IconButton
            disabled
            className={classes.iconButton}
            aria-label="search"
          >
            <SearchIcon />
          </IconButton>
          <Divider className={classes.divider} />
          <Tooltip title="Create new project">
            <IconButton
              aria-label="create"
              onClick={handleOpenCreateProjectDialog}
            >
              <AddCircleOutlineRoundedIcon />
            </IconButton>
          </Tooltip>
        </Paper>
      </div>
      <Grid container spacing={2}>
        {filteredProjects().map((project) => (
          <Grid key={project._id} container item xs={12}>
            <ListElement
              allSupervisors={getUserDataFromObject(data.users)}
              project={project}
            />
          </Grid>
        ))}
      </Grid>
      {getCreateProjectDialog()}
    </React.Fragment>
  );
};

export default List;
