import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@materasdial-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 400,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const ElementSelector = () => {
  const classes = useStyles();
  const [elementSelected, setElementSelected] = useState('');

  const handleElementSelected = (event) => {
    setElementSelected(event.target.value);
  };
  return (
    <Paper component="form" className={classes.root}>
      <FormControl fullWidth variant="outlined">
        <InputLabel id="search-from-list">Occupation</InputLabel>

        <Select
          labelId="search-from-list"
          id="search-from-list-selector"
          value={elementSelected}
          onChange={handleElementSelected}
          label="Occupation"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {}
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
      </FormControl>
    </Paper>
  );
};

export default ElementSelector;
