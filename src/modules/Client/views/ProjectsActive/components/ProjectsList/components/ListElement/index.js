import React, { useState } from 'react';
import {
  Typography,
  CardContent,
  Card,
  Chip,
  makeStyles,
  useTheme,
  useMediaQuery,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  Grid,
  TextField,
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { getDateFromUTC } from 'utils';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100%',
  },
  cardHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'flex-start',
    },
  },
  cardDescription: {
    display: 'flex',
    margin: theme.spacing(1.5, 0),
  },
  content: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  enterpriseLogo: {
    minWidth: 160,
    maxWidth: 160,
  },
  chipWrapper: {
    textAlign: 'center',
  },
  dateChip: {
    color: theme.palette.custom.white,
    fontWeight: 'bold',
    backgroundColor: theme.palette.error.light,
  },
  actionButtons: {
    marginLeft: 'auto',
    textAlign: 'center',
  },
  supervisors: {
    marginBottom: 10,
  },
  supervisorChip: {
    fontWeight: 'bold',
    backgroundColor: theme.palette.secondary.main,
    margin: theme.spacing(0.5),
  },
  withoutOverflow: {
    overflowY: 'initial',
  },
}));

const ListElement = ({
  allSupervisors,
  project: { _id, description, deadline, supervisors },
}) => {
  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const [
    openManageSupervisorsByProjectDialog,
    setOpenManageSupervisorsByProjectDialog,
  ] = useState(false);
  const supervisorsDataFromFb = useSelector(
    (state) => state.firestore.data.users
  );

  const showDetailsOfProject = () =>
    history.push(`/client/projects/active/${_id}`);

  const handleCloseManageSupervisorsByProjectDialog = () =>
    setOpenManageSupervisorsByProjectDialog(false);

  const getManageSupervisorsDialog = () => (
    <Dialog
      maxWidth="xs"
      open={openManageSupervisorsByProjectDialog}
      onClose={handleCloseManageSupervisorsByProjectDialog}
      aria-labelledby="manage-supervisors-dialog-title"
      aria-describedby="manage-supervisors-dialog-description"
    >
      <DialogTitle id="manage-supervisors-dialog-title">
        Manage the <b>supervisors</b> of this project
      </DialogTitle>
      <DialogContent className={classes.withoutOverflow}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Autocomplete
              multiple
              limitTags={2}
              size="small"
              options={allSupervisors}
              getOptionLabel={(supervisor) =>
                `${supervisor.names} ${supervisor.lastNames}`
              }
              // defaultValue={[top100Films[13], top100Films[12], top100Films[11]]}
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Type a supervisor name"
                  fullWidth
                  size="small"
                  variant="outlined"
                  type="text"
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              autoFocus
              fullWidth
              size="medium"
              color="primary"
              variant="contained"
              onClick={() => console.log('save supervisors')}
            >
              Save
            </Button>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );

  return (
    <React.Fragment>
      <Card className={classes.root}>
        <CardContent className={classes.content}>
          <div className={classes.cardHeader}>
            <Typography variant="h3">{description.title}</Typography>
            <div className={classes.chipWrapper}>
              <Typography variant="subtitle2">Expires</Typography>
              <Chip
                label={getDateFromUTC(deadline)}
                size={isMobile ? 'small' : 'medium'}
                className={classes.dateChip}
              />
            </div>
          </div>
          <div>
            <Typography variant="subtitle2" className={classes.cardDescription}>
              {description.content}
            </Typography>
          </div>
          <div className={classes.supervisors}>
            {supervisors.map(
              (uid) =>
                !!supervisorsDataFromFb[uid] && (
                  <Chip
                    key={uid}
                    size={isMobile ? 'small' : 'medium'}
                    label={`${supervisorsDataFromFb[uid].names} ${supervisorsDataFromFb[uid].lastNames}`}
                    className={classes.supervisorChip}
                  />
                )
            )}
          </div>
          <div className={classes.actionButtons}>
            {/* <Button
              variant="outlined"
              color="primary"
              style={{ margin: '0 5px' }}
              onClick={handleOpenManageSupervisorsByProjectDialog}
            >
              Manage supervisors
            </Button> */}
            <Button
              variant="outlined"
              color="primary"
              style={{ margin: '0 5px' }}
              onClick={showDetailsOfProject}
            >
              Details
            </Button>
          </div>
        </CardContent>
      </Card>
      {getManageSupervisorsDialog()}
    </React.Fragment>
  );
};

export default ListElement;
