import React, { useEffect } from 'react';
import withUserAsRole from 'hocs/withUserAsRole';
import ProjectsList from './components/ProjectsList';
import { useDispatch, useSelector } from 'react-redux';
import { getProjectsByClientRequested } from 'actions';

const ProjectsActive = () => {
  const dispatch = useDispatch();
  const { uid } = useSelector((state) => state.firebase.auth);

  useEffect(() => {
    dispatch(getProjectsByClientRequested(uid));
  }, [dispatch, uid]);

  return <ProjectsList />;
};

export default withUserAsRole('client', ProjectsActive);
