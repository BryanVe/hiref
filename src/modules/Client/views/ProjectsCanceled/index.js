import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';

const ProjectsCanceled = () => {
  return <div>Client projects canceled</div>;
};

export default withUserAsRole('client', ProjectsCanceled);
