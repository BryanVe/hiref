import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';

const ProjectsCompleted = () => {
  return <div>Client projects completed</div>;
};

export default withUserAsRole('client', ProjectsCompleted);
