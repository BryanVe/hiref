import React, { useState } from 'react';
import withUserAsRole from 'hocs/withUserAsRole';

import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import {
  makeStyles,
  Typography,
  IconButton,
  CardContent,
  Grid,
  Card,
  TextField,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  useTheme,
  useMediaQuery,
} from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useSelector, useDispatch } from 'react-redux';
import { createJobOfferByClientRequested } from 'actions';

const initialRoleState = {
  description: {
    content: '',
    title: '',
  },
  quantity: '',
  remuneration: '',
};

const useStyles = makeStyles((theme) => ({
  goBack: {
    display: 'flex',
    alignItems: 'center',
  },
  withoutOverflow: {
    overflowY: 'initial',
  },
}));

const ProjectCreateJobOffer = () => {
  const { commercial_name } = useSelector((state) => state.firebase.profile);
  const { projectId } = useParams();
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const [newRoleData, setNewRoleData] = useState(initialRoleState);
  const [openCreateNewRole, setOpenCreateNewRole] = useState(false);
  const initialJobOfferState = {
    commercialName: commercial_name,
    code: '',
    deadline: null,
    description: {
      content: '',
      title: '',
    },
    occupations: ['Ocupation 1'],
    roles: [],
  };
  const [newJobOfferData, setNewJobOfferData] = useState(initialJobOfferState);

  const handleOpenCreateNewRole = () => setOpenCreateNewRole(true);
  const handleCloseCreateNewRole = () => setOpenCreateNewRole(false);

  const createNewRole = () => {
    setNewJobOfferData({
      ...newJobOfferData,
      roles: [...newJobOfferData.roles, newRoleData],
    });
    setNewRoleData(initialRoleState);
    handleCloseCreateNewRole();
  };

  const createNewJobOffer = () => {
    dispatch(createJobOfferByClientRequested(projectId, newJobOfferData));
    setNewJobOfferData(initialJobOfferState);
    history.goBack();
  };

  const getCreateNewRoleDialog = () => (
    <Dialog
      maxWidth="sm"
      open={openCreateNewRole}
      onClose={handleCloseCreateNewRole}
      aria-labelledby="create-project-dialog-title"
      aria-describedby="create-project-dialog-description"
    >
      <DialogTitle id="create-project-dialog-title">
        Create a <b>new role</b>
      </DialogTitle>
      <DialogContent className={classes.withoutOverflow}>
        <Grid container spacing={2}>
          <Grid item container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField
                value={newRoleData.description.title}
                onChange={(event) =>
                  setNewRoleData({
                    ...newRoleData,
                    description: {
                      ...newRoleData.description,
                      title: event.target.value,
                    },
                  })
                }
                fullWidth
                size="small"
                label="Title"
                variant="outlined"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={3}>
              <TextField
                value={newRoleData.quantity}
                onChange={(event) =>
                  setNewRoleData({
                    ...newRoleData,
                    quantity: parseInt(event.target.value),
                  })
                }
                fullWidth
                size="small"
                label="Quantity"
                variant="outlined"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={3}>
              <TextField
                value={newRoleData.remuneration}
                onChange={(event) =>
                  setNewRoleData({
                    ...newRoleData,
                    remuneration: parseInt(event.target.value),
                  })
                }
                fullWidth
                size="small"
                label="Remuneration"
                variant="outlined"
                type="text"
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              size="small"
              value={newRoleData.description.content}
              onChange={(event) =>
                setNewRoleData({
                  ...newRoleData,
                  description: {
                    ...newRoleData.description,
                    content: event.target.value,
                  },
                })
              }
              label="Description"
              multiline
              rows={5}
              variant="outlined"
              type="text"
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          fullWidth={isMobile}
          className={classes.saveButton}
          onClick={createNewRole}
          variant="contained"
          color="primary"
          autoFocus
        >
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
  return (
    <React.Fragment>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: 20,
        }}
      >
        <div className={classes.goBack}>
          <IconButton
            onClick={() => history.push(`/client/projects/active/${projectId}`)}
          >
            <ArrowBackRoundedIcon />
          </IconButton>
          <Typography variant="h4" style={{ marginLeft: 5 }}>
            Back
          </Typography>
        </div>
        <Button variant="contained" color="primary" onClick={createNewJobOffer}>
          Save job offer
        </Button>
      </div>
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item container spacing={2}>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  value={newJobOfferData.description.title}
                  onChange={(event) =>
                    setNewJobOfferData({
                      ...newJobOfferData,
                      description: {
                        ...newJobOfferData.description,
                        title: event.target.value,
                      },
                    })
                  }
                  size="small"
                  label="Title"
                  variant="outlined"
                  type="text"
                />
              </Grid>
              <Grid item xs={12} md={3}>
                <TextField
                  value={newJobOfferData.code}
                  onChange={(event) =>
                    setNewJobOfferData({
                      ...newJobOfferData,
                      code: event.target.value,
                    })
                  }
                  fullWidth
                  size="small"
                  label="Code"
                  variant="outlined"
                  type="text"
                />
              </Grid>
              <Grid item xs={12} md={3}>
                <KeyboardDatePicker
                  fullWidth
                  value={newJobOfferData.deadline}
                  size="small"
                  inputVariant="outlined"
                  clearable
                  placeholder="Deadline"
                  onChange={(date) =>
                    setNewJobOfferData({
                      ...newJobOfferData,
                      deadline: date.format('YYYY-MM-DD'),
                    })
                  }
                  minDate={new Date()}
                  format="DD/MM/YYYY"
                />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <TextField
                value={newJobOfferData.description.content}
                onChange={(event) =>
                  setNewJobOfferData({
                    ...newJobOfferData,
                    description: {
                      ...newJobOfferData.description,
                      content: event.target.value,
                    },
                  })
                }
                fullWidth
                size="small"
                label="Description"
                multiline
                rows={5}
                variant="outlined"
                type="text"
              />
            </Grid>
            <Grid item xs={12} container justify="flex-end">
              <Button
                onClick={handleOpenCreateNewRole}
                variant="contained"
                color="secondary"
              >
                Add role
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      {newJobOfferData.roles.map((role, index) => (
        <Card key={index} style={{ marginTop: 20 }}>
          <CardContent>
            <Typography variant="h3">{role.description.title}</Typography>
            <Typography variant="h4">Description:</Typography>
            <Typography variant="h5">{role.description.content}</Typography>
            <Typography variant="h5">Quantity: {role.quantity}</Typography>
            <Typography variant="h5">
              Remuneration: {role.remuneration}
            </Typography>
          </CardContent>
        </Card>
      ))}
      {getCreateNewRoleDialog()}
    </React.Fragment>
  );
};

export default withUserAsRole('client', ProjectCreateJobOffer);
