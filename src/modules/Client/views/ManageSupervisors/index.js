import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';
import TableData from './components/TableData';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  headerTitle: {
    marginBottom: 20,
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
  },
}));

const ManageSupervisors = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Typography variant="h1" className={classes.headerTitle}>
        Manage supervisors
      </Typography>
      <TableData />
    </React.Fragment>
  );
};

export default withUserAsRole('client', ManageSupervisors);
