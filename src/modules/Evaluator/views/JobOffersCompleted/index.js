import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';

const JobOffersCompleted = () => {
  return <div>JobOffersCompleted</div>;
};

export default withUserAsRole('evaluator', JobOffersCompleted);
