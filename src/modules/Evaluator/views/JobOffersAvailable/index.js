import React, { useEffect } from 'react';
import withUserAsRole from 'hocs/withUserAsRole';
import { useDispatch } from 'react-redux';
import { getAllJobOffersRequested } from 'actions';

const JobOffersAvailable = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllJobOffersRequested());
  }, [dispatch]);

  return <div>JobOffersAvailable</div>;
};

export default withUserAsRole('evaluator', JobOffersAvailable);
