import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';

const JobOffersInEvaluation = () => {
  return <div>JobOffersInEvaluation</div>;
};

export default withUserAsRole('evaluator', JobOffersInEvaluation);
