import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';
import { useFirebase } from 'react-redux-firebase';
import { useHistory } from 'react-router-dom';

const Profile = () => {
  const firebase = useFirebase();
  const history = useHistory();

  return (
    <React.Fragment>
      <h1>My profile as admin</h1>
      <button
        onClick={() => {
          firebase
            .logout()
            .then(() => {
              history.push('/auth/signin');
            })
            .catch((err) => console.log(err.message));
        }}
      >
        Sign out
      </button>
    </React.Fragment>
  );
};

export default withUserAsRole('admin', Profile);
