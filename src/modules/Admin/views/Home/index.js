import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';
import { Redirect } from 'react-router-dom';

const AdminHome = () => {
  return <Redirect to="/admin/manage-users" />;
};

export default withUserAsRole('admin', AdminHome);
