import React from 'react';
import withUserAsRole from 'hocs/withUserAsRole';

const Profile = () => {
  return <h1>My profile as aspirant</h1>;
};

export default withUserAsRole('aspirant', Profile);
