import React from 'react';
import { useFirebase } from 'react-redux-firebase';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import withUserAsRole from 'hocs/withUserAsRole';

const AspirantHome = () => {
  const history = useHistory();
  const firebase = useFirebase();
  const profile = useSelector((state) => state.firebase.profile);

  return (
    <div>
      <h1>Aspirant Home View</h1>
      <h1>{JSON.stringify(profile)}</h1>

      <button
        onClick={() => {
          firebase
            .logout()
            .then(() => {
              history.push('/auth/signin');
            })
            .catch((err) => console.log(err.message));
        }}
      >
        Sign out
      </button>
    </div>
  );
};

export default withUserAsRole('aspirant', AspirantHome);
