import React, { useEffect } from 'react';
import ListJobOffers from './components/ListJobOffers';
import withUserAsRole from 'hocs/withUserAsRole';
import { makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getJobOffersByOccupationsRequested } from 'actions';
const useStyles = makeStyles((theme) => ({
  root: {
    // margin: '0 auto',
    // padding: theme.spacing(3),
  },
}));

const JobOffersAvailable = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { occupations } = useSelector((state) => state.firebase.profile);

  useEffect(() => {
    dispatch(getJobOffersByOccupationsRequested(occupations));
  }, [dispatch, occupations]);

  return (
    <div className={classes.root}>
      <ListJobOffers />
    </div>
  );
};

export default withUserAsRole('aspirant', JobOffersAvailable);
