import React, { useState } from 'react';
import {
  Typography,
  CardContent,
  Card,
  Chip,
  makeStyles,
  CardMedia,
  useTheme,
  useMediaQuery,
  Button,
  Drawer,
  Avatar,
  Grid,
  Tabs,
  Tab,
  Box,
  AppBar,
} from '@material-ui/core';

import imageNotAvailable from 'assets/images/imageNotAvailable.png';
import { getDateFromUTC } from 'utils';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100%',
  },
  cardHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  cardDescription: {
    margin: theme.spacing(1.5, 0),
  },
  content: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  clientLogo: {
    minWidth: 160,
    maxWidth: 160,
  },

  deadlineChipWrapper: {
    textAlign: 'center',
  },
  dateChip: {
    color: theme.palette.custom.white,
    fontWeight: 'bold',
    backgroundColor: theme.palette.error.light,
  },
  detailsButton: {
    marginLeft: 'auto',
  },
  detailsDrawerContent: {
    backgroundColor: theme.palette.custom.white,
    width: '70vw',
    [theme.breakpoints.down('sm')]: {
      width: '90vw',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100vw',
    },
  },
  clientLogoDetails: {
    width: 150,
    height: 150,
    [theme.breakpoints.down('sm')]: {
      width: 120,
      height: 120,
    },
  },
  applicantsVsTotalChip: {
    backgroundColor: theme.palette.success.light,
    color: theme.palette.custom.white,
    fontWeight: 'bold',
    marginTop: theme.spacing(1),
  },
  contentTab: {
    backgroundColor: theme.palette.custom.white,
  },
}));

const getNumberOfAppliesVsTotal = (roles) => {
  const versus = {
    applicants: 0,
    total: 0,
  };
  for (let role of roles) {
    versus.applicants += role.applicants.length;
    versus.total += role.quantity;
  }
  return `${versus.applicants} / ${versus.total}`;
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const ListElement = ({
  jobOffer: { description, deadline, occupations, roles, status },
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('md'));
  const [openDetailsDrawer, setOpenDetailsDrawer] = useState(false);
  const [roleTab, setRoleTab] = useState(0);

  const [expanded, setExpanded] = React.useState('panel1');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const handleOpenDetailsDrawer = () => setOpenDetailsDrawer(true);
  const handleCloseDetailsDrawer = () => setOpenDetailsDrawer(false);
  const handleChangeRoleTab = (event, tabIndex) => setRoleTab(tabIndex);

  const getDrawerContent = () => (
    <div className={classes}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={4} md={3}>
          <Avatar
            alt="Client logo"
            src={imageNotAvailable}
            variant="square"
            className={classes.clientLogoDetails}
          />
        </Grid>
        <Grid item xs={12} sm={8} md={9}>
          <Card>
            <CardContent>
              <Typography variant="h2">{description.title}</Typography>
              {/* <Typography>{clientName}</Typography> */}
              {getApplicantsVsTotalChip()}
              <Typography variant="h3">Description:</Typography>
              <Typography variant="h4">{description.content}</Typography>
              <AppBar position="static" color="default">
                <Tabs
                  value={roleTab}
                  onChange={handleChangeRoleTab}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="scrollable"
                  scrollButtons="auto"
                >
                  {roles.map(({ _id, description }, index) => (
                    <Tab
                      key={_id}
                      label={description.title}
                      {...a11yProps(index)}
                    />
                  ))}
                </Tabs>
              </AppBar>
              {roles.map(
                ({ _id, description, quantity, remuneration }, index) => (
                  <TabPanel key={_id} value={roleTab} index={index}>
                    {description.content}
                    {quantity} {remuneration}
                  </TabPanel>
                )
              )}
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );

  const getApplicantsVsTotalChip = () => (
    <Chip
      size={isMobile ? 'small' : 'medium'}
      label={getNumberOfAppliesVsTotal(roles)}
      className={classes.applicantsVsTotalChip}
    />
  );

  return (
    <React.Fragment>
      <Card className={classes.root}>
        {!isMobile && (
          <CardMedia
            className={classes.clientLogo}
            image={imageNotAvailable}
            title="clientImage"
          />
        )}
        <CardContent className={classes.content}>
          <div className={classes.cardHeader}>
            <div>
              <Typography variant="h3">{description.title}</Typography>
              {getApplicantsVsTotalChip()}
            </div>
            <div className={classes.deadlineChipWrapper}>
              <Typography variant="subtitle2">Expires</Typography>
              <Chip
                label={getDateFromUTC(deadline)}
                size={isMobile ? 'small' : 'medium'}
                className={classes.dateChip}
              />
            </div>
          </div>
          <Typography variant="subtitle2" className={classes.cardDescription}>
            {description.content}
          </Typography>
          <Button
            variant="outlined"
            color="primary"
            className={classes.detailsButton}
            onClick={handleOpenDetailsDrawer}
          >
            Details
          </Button>
        </CardContent>
      </Card>
      <Drawer
        PaperProps={{
          className: classes.detailsDrawerContent,
        }}
        variant="temporary"
        anchor="right"
        open={openDetailsDrawer}
        onClose={handleCloseDetailsDrawer}
      >
        {getDrawerContent()}
      </Drawer>
    </React.Fragment>
  );
};

export default ListElement;
