import React, { useState } from 'react';
import {
  makeStyles,
  Grid,
  Typography,
  MenuItem,
  Paper,
  FormControl,
  InputLabel,
  Select,
} from '@material-ui/core';
import { useSelector } from 'react-redux';
import Loading from 'components/Loading';
import JobOffer from './components/JobOffer';

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  headerTitle: {
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
      marginBottom: 10,
    },
  },
  elementSelector: {
    width: 400,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
}));

const getJobOffersByOccupation = (jobOffers, occupation) => {
  if (occupation === 'all') return jobOffers;

  let myJobOffers = [];
  for (let jobOffer of jobOffers) {
    if (jobOffer.occupations.includes(occupation)) myJobOffers.push(jobOffer);
  }

  return myJobOffers;
};

const ListJobOFfers = () => {
  const classes = useStyles();
  const { occupations } = useSelector((state) => state.firebase.profile);
  const { loading, data: jobOffers } = useSelector((state) => state.jobOffers);
  const [elementSelected, setElementSelected] = useState('all');

  const handleElementSelected = (event) => {
    setElementSelected(event.target.value);
  };

  return loading ? (
    <Loading />
  ) : (
    <React.Fragment>
      <div className={classes.header}>
        <Typography variant="h1" className={classes.headerTitle}>
          Job offers available
        </Typography>
        <Paper component="form" className={classes.elementSelector}>
          <FormControl fullWidth variant="outlined">
            <InputLabel id="search-from-list">Occupation</InputLabel>

            <Select
              labelId="search-from-list"
              id="search-from-list-selector"
              value={elementSelected}
              onChange={handleElementSelected}
              label="Occupation"
            >
              <MenuItem value="all">All</MenuItem>
              {occupations.map((occupation) => (
                <MenuItem key={occupation} value={occupation}>
                  {occupation}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Paper>
      </div>
      <Grid container spacing={2}>
        {getJobOffersByOccupation(jobOffers, elementSelected).map(
          (jobOffer) => (
            <Grid key={jobOffer._id} container item xs={12}>
              <JobOffer jobOffer={jobOffer} />
            </Grid>
          )
        )}
      </Grid>
    </React.Fragment>
  );
};

export default ListJobOFfers;
