import React from 'react';

import {
  AdminLayout,
  AspirantLayout,
  CommercialAreaLayout,
  ErrorLayout,
  AuthLayout,
  ClientLayout,
  EvaluatorLayout,
} from './layouts';
import {
  AspirantProfile,
  AspirantJobOffersAvailable,
} from 'modules/Aspirant/views';
import { AdminProfile } from 'modules/Admin/views';
import { CommercialAreaManageClients } from 'modules/CommercialArea/views';
import {
  EvaluatorJobOffersAvailable,
  EvaluatorJobOffersCompleted,
  EvaluatorJobOffersInEvaluation,
} from 'modules/Evaluator/views';
import {
  ClientProjectsActive,
  ClientProjectActiveDetails,
  ClientProjectsCompleted,
  ClientProjectsCanceled,
  ClientManageSupervisors,
  ClientPayMembership,
  ClientProjectCreateJobOffer,
} from 'modules/Client/views';
import { Redirect } from 'react-router-dom';
import { Error401, Error404, SignIn, SignUp } from 'views';

export default [
  {
    path: '/auth',
    layout: AuthLayout,
    views: [
      {
        path: '/auth/signin',
        component: SignIn,
      },
      {
        path: '/auth/signup',
        component: SignUp,
      },
    ],
  },
  {
    path: '/admin',
    layout: AdminLayout,
    views: [
      {
        path: '/admin',
        component: () => <Redirect to="/admin/manage-users" />,
      },

      {
        path: '/admin/manage-users',
        component: AdminProfile,
      },
    ],
  },
  {
    path: '/aspirant',
    layout: AspirantLayout,
    views: [
      {
        path: '/aspirant',
        // component: AspirantHome,
        component: () => <Redirect to="/aspirant/job-offers/available" />,
      },
      {
        path: '/aspirant/job-offers/available',
        component: AspirantJobOffersAvailable,
      },
      {
        path: '/aspirant/job-offers/applied',
        component: AspirantProfile,
      },
      // {
      //   path: '/aspirant/job-offers/rejected',
      //   component: AspirantProfile,
      // },
    ],
  },
  {
    path: '/commercial-area',
    layout: CommercialAreaLayout,
    views: [
      {
        path: '/commercial-area',
        // component: AspirantHome,
        component: () => <Redirect to="/commercial-area/manage-clients" />,
      },
      {
        path: '/commercial-area/manage-clients',
        component: CommercialAreaManageClients,
      },
    ],
  },
  {
    path: '/client',
    layout: ClientLayout,
    views: [
      {
        path: '/client',
        // component: AspirantHome,
        component: () => <Redirect to="/client/projects/active" />,
      },
      {
        path: '/client/projects/active',
        component: ClientProjectsActive,
      },
      {
        path: '/client/projects/active/:projectId',
        component: ClientProjectActiveDetails,
      },
      {
        path: '/client/projects/active/:projectId/create-joboffer',
        component: ClientProjectCreateJobOffer,
      },
      {
        path: '/client/projects/completed',
        component: ClientProjectsCompleted,
      },
      {
        path: '/client/projects/canceled',
        component: ClientProjectsCanceled,
      },
      {
        path: '/client/manage-supervisors',
        component: ClientManageSupervisors,
      },
      {
        path: '/client/pay-membership',
        component: ClientPayMembership,
      },
    ],
  },
  {
    path: '/evaluator',
    layout: EvaluatorLayout,
    views: [
      {
        path: '/evaluator',
        component: () => <Redirect to="/evaluator/job-offers/available" />,
      },
      {
        path: '/evaluator/job-offers/available',
        component: EvaluatorJobOffersAvailable,
      },
      {
        path: '/evaluator/job-offers/in-evaluation',
        component: EvaluatorJobOffersInEvaluation,
      },
      {
        path: '/evaluator/job-offers/completed',
        component: EvaluatorJobOffersCompleted,
      },
    ],
  },
  {
    path: '/error',
    layout: ErrorLayout,
    views: [
      {
        path: '/error/401',
        component: Error401,
      },

      {
        path: '/error/404',
        component: Error404,
      },
    ],
  },
];
