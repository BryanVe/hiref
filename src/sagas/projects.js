import { put, call, takeLatest } from 'redux-saga/effects';
import {
  GET_PROJECTS_BY_CLIENT,
  CREATE_PROJECT_BY_CLIENT,
  GET_PROJECT_BY_ID,
} from 'constants/index';
import {
  getProjectsByClientSuccess,
  getProjectsByClientError,
  createProjectByClientSuccess,
  createProjectByClientError,
  getProjectByIdSuccess,
  getProjectByIdError,
} from 'actions';
import { Get, Post } from 'utils';

function* getProjectsByClient({ payload: { idClient } }) {
  try {
    const {
      message: { result },
    } = yield call(Get, `/projects/${idClient}`);
    yield put(getProjectsByClientSuccess(result));
  } catch (error) {
    const { message } = error.response;
    yield put(getProjectsByClientError(message));
  }
}

function* createProjectByClient({ payload: { idClient, newProject } }) {
  try {
    const {
      message: { result },
    } = yield call(Post, `/projects/${idClient}`, {
      args: newProject,
    });
    yield put(createProjectByClientSuccess(result));
  } catch (error) {
    const { message } = error.response;
    yield put(createProjectByClientError(message));
  }
}

function* getProjectById({ payload: { projectId } }) {
  try {
    const {
      message: { result },
    } = yield call(Get, `/projects/getById/${projectId}`);
    yield put(getProjectByIdSuccess(result));
  } catch (error) {
    const { message } = error.response;
    yield put(getProjectByIdError(message));
  }
}

export function* getProjectsByClientSaga() {
  yield takeLatest(GET_PROJECTS_BY_CLIENT.REQUESTED, getProjectsByClient);
}

export function* createProjectByClientSaga() {
  yield takeLatest(CREATE_PROJECT_BY_CLIENT.REQUESTED, createProjectByClient);
}

export function* getProjectByIdSaga() {
  yield takeLatest(GET_PROJECT_BY_ID.REQUESTED, getProjectById);
}
