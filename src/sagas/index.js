import { all, fork } from 'redux-saga/effects';
import {
  getAllJobOffersSaga,
  getJobOffersByOccupationsSaga,
  getJobOffersByProjectSaga,
  createJobOfferByClientSaga,
} from './jobOffers';
import {
  createProjectByClientSaga,
  getProjectsByClientSaga,
  getProjectByIdSaga,
} from './projects';

function* rootSaga() {
  yield all([
    fork(getJobOffersByOccupationsSaga),
    fork(getProjectByIdSaga),
    fork(getAllJobOffersSaga),
    fork(getProjectsByClientSaga),
    fork(createProjectByClientSaga),
    fork(getJobOffersByProjectSaga),
    fork(createJobOfferByClientSaga),
  ]);
}

export default rootSaga;
