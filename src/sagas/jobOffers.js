import { put, takeLatest, call } from 'redux-saga/effects';
import {
  GET_JOB_OFFERS_BY_OCCUPATIONS,
  GET_ALL_JOB_OFFERS,
  GET_JOB_OFFERS_BY_PROJECT,
  CREATE_JOB_OFFER_BY_CLIENT,
} from 'constants/index';
import {
  getJobOffersByOccupationsSuccess,
  getJobOffersByOccupationsError,
  getAllJobOffersSuccess,
  getAllJobOffersError,
  getJobOffersByProjectSuccess,
  getJobOffersByProjectError,
  createJobOfferByClientSuccess,
  createJobOfferByClientError,
} from 'actions';
import { Post, Get } from 'utils';

// const getJobOffersByOccupations = (jobOffers, myOccupations) => {
//   let myJobOffers = [];

//   for (let jobOffer of jobOffers) {
//     for (let occupation of jobOffer.occupations) {
//       if (myOccupations.includes(occupation)) {
//         myJobOffers.push(jobOffer);
//         break;
//       }
//     }
//   }

//   return myJobOffers;
// };

function* getJobOffersByOccupations({ payload: { occupations } }) {
  try {
    const {
      message: { result },
    } = yield call(Post, '/jobOffers/getAll/byOccupations', {
      args: {
        occupations,
      },
    });
    yield put(getJobOffersByOccupationsSuccess(result));
  } catch (error) {
    const { message } = error.response;
    yield put(getJobOffersByOccupationsError(message));
  }
}

function* getAllJobOffers() {
  try {
    const {
      message: { result },
    } = yield call(Get, '/jobOffers');
    yield put(getAllJobOffersSuccess(result));
  } catch (error) {
    const { message } = error.response;
    yield put(getAllJobOffersError(message));
  }
}

function* getJobOffersByProject({ payload: { projectId } }) {
  try {
    const {
      message: { result },
    } = yield call(Get, `/jobOffers/getAll/byProject/${projectId}`);
    yield put(getJobOffersByProjectSuccess(result));
  } catch (error) {
    const { message } = error.response;
    yield put(getJobOffersByProjectError(message));
  }
}

function* createJobOfferByClient({ payload: { projectId, newJobOffer } }) {
  try {
    const {
      message: { result },
    } = yield call(Post, `/jobOffers/store/${projectId}`, {
      args: newJobOffer,
    });
    yield put(createJobOfferByClientSuccess(result));
  } catch (error) {
    const { message } = error.response;
    yield put(createJobOfferByClientError(message));
  }
}

export function* getJobOffersByOccupationsSaga() {
  yield takeLatest(
    GET_JOB_OFFERS_BY_OCCUPATIONS.REQUESTED,
    getJobOffersByOccupations
  );
}

export function* getAllJobOffersSaga() {
  yield takeLatest(GET_ALL_JOB_OFFERS.REQUESTED, getAllJobOffers);
}

export function* getJobOffersByProjectSaga() {
  yield takeLatest(GET_JOB_OFFERS_BY_PROJECT.REQUESTED, getJobOffersByProject);
}

export function* createJobOfferByClientSaga() {
  yield takeLatest(
    CREATE_JOB_OFFER_BY_CLIENT.REQUESTED,
    createJobOfferByClient
  );
}
