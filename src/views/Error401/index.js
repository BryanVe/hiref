import React from 'react';
import { useHistory } from 'react-router-dom';

const Error401 = () => {
  const history = useHistory();

  return (
    <React.Fragment>
      <h1>No estas autorizado para ver esta ruta</h1>
      <button onClick={() => history.push('/')}>Go to home</button>
    </React.Fragment>
  );
};

export default Error401;
