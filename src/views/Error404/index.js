import React from 'react';
import { useHistory } from 'react-router-dom';

const Erro404 = () => {
  const history = useHistory();

  return (
    <React.Fragment>
      <h1>404 - Not Found</h1>
      <button onClick={() => history.push('/')}>Go to home</button>
    </React.Fragment>
  );
};

export default Erro404;
