import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/core';
import { Provider as StoreProvider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { createFirestoreInstance } from 'redux-firestore';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import WebFont from 'webfontloader';
import firebase from 'firebase/app';
import firebaseConfig from 'firebase/config';
import moment from 'moment';
import 'firebase/auth';
import 'firebase/firestore';
import 'moment/locale/es-do';

import configureStore from 'store';
import App from 'App';
import theme from 'theme';
import './index.css';

const locale = 'en';

const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true,
};

firebase.initializeApp(firebaseConfig);
firebase.firestore();

const store = configureStore();

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
};

WebFont.load({
  google: {
    families: ['Quicksand:500,700', 'sans-serif', 'Lexend Deca:400'],
  },
});

ReactDOM.render(
  <MuiPickersUtilsProvider
    utils={MomentUtils}
    libInstance={moment}
    locale={locale}
  >
    <StoreProvider store={store}>
      <ReactReduxFirebaseProvider {...rrfProps}>
        <ThemeProvider theme={theme}>
          <Router>
            <App />
          </Router>
        </ThemeProvider>
      </ReactReduxFirebaseProvider>
    </StoreProvider>
  </MuiPickersUtilsProvider>,

  document.getElementById('root')
);
